DESCRIPTION = "Clutter based widget library"
LICENSE = "LGPLv2.1"
PR = "r2"

DEPENDS = "clutter-1.0 gdk-pixbuf"

inherit autotools

SRC_URI = "https://github.com/clutter-project/${PN}/archive/${PV}.tar.gz \
	   file://0001-Do-not-compile-mx-image.patch \
	   file://0002-Drop-gtk-doc.patch \
	   file://0003-clutter-actor-contains-does-not-exist-in-clutter-1.2.patch \
	   file://20140324-implement-mx-texture-cache-clear.patch \
	   file://20150209-mx_style_sheet_add_from_data.patch \
	   "

SRC_URI[md5sum] = "838925be24a97d574ad8da67402dccbb"
SRC_URI[sha256sum] = "eb85752753e8084392b645693f1108e27608ec45f39360b086dd5a69f4554845"


LIC_FILES_CHKSUM = "file://COPYING.LIB;md5=fbc093901857fcd118f065f900982c24 \
		    "
EXTRA_OECONF = "--disable-introspection		\
		--disable-gtk-widgets		\
		--disable-gtk-doc		\
		--enable-gtk-doc=no		\
		--disable-gtk-doc-html		\
	        --disable-gtk-widgets		\
		--with-winsys=none		\
		--without-clutter-imcontext	\
		--without-clutter-gesture	\
		--without-startup-notification	\
		"
