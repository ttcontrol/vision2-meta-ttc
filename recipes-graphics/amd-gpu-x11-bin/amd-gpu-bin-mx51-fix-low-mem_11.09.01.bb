# Copyright (C) 2011, 2012 Freescale
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "GPU driver and apps for frambuffer on mx51"

AMD_MIRROR = "${TTC_MIRROR}"
S = "${WORKDIR}/amd-gpu-bin-mx51-11.09.01"

include amd-gpu-mx51.inc

SRC_URI[md5sum] = "5f556ec790ee323c197d661053500efc"
SRC_URI[sha256sum] = "c01c241251272fbf45b4ab0fda2c8383d0ea4bab559bc6d7ca3a3208380c6cb5"

RCONFLICTS_${PN} = "amd-gpu-x11-bin-mx51"

COMPATIBLE_MACHINE = "(mx5)"
