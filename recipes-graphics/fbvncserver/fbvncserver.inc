SUMMARY = "Framebuffer VNC Server"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://README;md5=9c25bffb41ebd605ccdb08c16de27fa2"

SRC_URI = "${TTC_GIT}/fbvncserver.git;protocol=${TTC_GIT_PROTO} \
          "
S = "${WORKDIR}/git"

DEPENDS = "jpeg dbus"

FILES_${PN} =+ "${datadir}/*"

inherit autotools
