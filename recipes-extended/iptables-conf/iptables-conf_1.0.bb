SUMMARY = "Extra machine specific configuration files"
DESCRIPTION = "Extra machine specific configuration files for iptables"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r1"

SRC_URI = " \
       file://default.rules \
"


do_install() {
    install -d ${D}${sysconfdir}/iptables

    install -m 0644 ${WORKDIR}/default.rules   ${D}${sysconfdir}/iptables
}

FILES_${PN} = "${sysconfdir}/iptables"
RDEPENDS_${PN} = "iptables"
