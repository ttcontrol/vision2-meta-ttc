SUMMARY = "Extra machine specific configuration files for ppp"
DESCRIPTION = "Extra machine specific configuration files for using ppp with the vision2."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
					file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
COMPATIBLE_MACHINE = "(vision2)"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-extraconf:"

PR = "r1"

SRC_URI = "file://etc/chatscripts/apn.none.default \
			file://etc/chatscripts/pin.none.default \
			file://etc/chatscripts/pin.defined \
			file://etc/chatscripts/apn.defined \
			file://etc/vira \
			file://etc/inet \
			file://etc/ppp_on_boot \
			file://etc/mobile-auth \
			file://etc/mobile-noauth \
			file://etc/chatscripts/inet-connect.chat \
			file://etc/chatscripts/inet-disconnect.chat \
			"

do_install() {

	install -d ${D}${sysconfdir}/ppp
	install -m 0644 ${WORKDIR}/etc/vira ${D}${sysconfdir}/ppp/vira
	install -m 0644 ${WORKDIR}/etc/inet ${D}${sysconfdir}/ppp/inet
	install -m 0644 ${WORKDIR}/etc/mobile-auth ${D}${sysconfdir}/ppp/mobile-auth
	install -m 0644 ${WORKDIR}/etc/mobile-noauth ${D}${sysconfdir}/ppp/mobile-noauth
	install -m 0755 ${WORKDIR}/etc/ppp_on_boot ${D}${sysconfdir}/ppp/ppp_on_boot
	
	install -d ${D}${sysconfdir}/ppp/peers
	ln -s /etc/ppp/mobile-noauth ${D}${sysconfdir}/ppp/peers/provider

	install -d ${D}${sysconfdir}/ppp/chatscripts
	install -m 0644 ${WORKDIR}/etc/chatscripts/apn.none.default ${D}${sysconfdir}/ppp/chatscripts/apn.none.default
	install -m 0644 ${WORKDIR}/etc/chatscripts/pin.none.default ${D}${sysconfdir}/ppp/chatscripts/pin.none.default
	install -m 0644 ${WORKDIR}/etc/chatscripts/apn.defined ${D}${sysconfdir}/ppp/chatscripts/apn.defined
	install -m 0644 ${WORKDIR}/etc/chatscripts/pin.defined ${D}${sysconfdir}/ppp/chatscripts/pin.defined
	install -m 0644 ${WORKDIR}/etc/chatscripts/inet-connect.chat ${D}${sysconfdir}/ppp/chatscripts/inet-connect.chat
	install -m 0644 ${WORKDIR}/etc/chatscripts/inet-disconnect.chat ${D}${sysconfdir}/ppp/chatscripts/inet-disconnect.chat
	
	ln -s /etc/ppp/chatscripts/pin.none.default ${D}${sysconfdir}/ppp/chatscripts/pin
	ln -s /etc/ppp/chatscripts/apn.none.default ${D}${sysconfdir}/ppp/chatscripts/apn
	
	ln -s /var/run/resolv.conf  ${D}${sysconfdir}/resolv.conf
}

FILES_${PN} = "${sysconfdir}/ppp ${sysconfdir}/resolv.conf"
RDEPENDS_${PN} = "ppp"

#CONFFILES_${PN} += "${sysconfdir}/ppp/vira ${sysconfdir}/ppp/inet ${sysconfdir}/ppp/mobile-auth ${sysconfdir}/ppp/mobile-noauth ${sysconfdir}/resolv.conf"





