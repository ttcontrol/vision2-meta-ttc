DESCRIPTION = "Mgetty - Modem TTY daemon"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=fd3b2e6132b4ff4cb475558807ded25f"

PR = "r1"
COMPATIBLE_MACHINE = "vision2"

SRC_URI = "file://${PN}-${PV}.tar.gz \
           file://0001-Makefile-cross-compile.patch \
           file://0002-use-flow-control-settings.patch \
           file://0003-skip-loginconf-permissions-check.patch \
           file://0004-mgetty-conf.patch \
           file://0005-mgetty-run.patch \
           file://0006-policy-h.patch \
           file://0007-login-conf.patch"

SRC_URI[md5sum] = "4df2eb47bd6d5318d3d642572ab56e51"
SRC_URI[sha256sum] = "6ff8cbc4f8aacd3dd7f247ff96715141bc6173745ea916dd98b8381a85ecdf0e"

S = "${WORKDIR}/${PN}-${PV}"

PARALLEL_MAKE = ""

#FILES_${PN} += "/rts ${bindir} ${libdir}"
#FILES_${PN}-dbg += "/rts/.debug ${libdir}/.debug"
#FILES_${PN}-dev = ""

do_install() {
	install -d ${D}${base_sbindir}
	install -d ${D}${sysconfdir}
        install -m 0775 ${S}/mgetty ${D}${base_sbindir}/mgetty
	install -m 0775 ${S}/mgetty.run ${D}${base_sbindir}/mgetty.run
	install -m 0664 ${S}/mgetty.conf ${D}${sysconfdir}/mgetty.conf
	install -m 0600 ${S}/login.conf ${D}${sysconfdir}/login.conf
}
