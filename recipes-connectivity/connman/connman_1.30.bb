PACKAGECONFIG = "wifi"

require connman.inc

#            file://build-create-dirs-before-putting-files-in-them.patch
SRC_URI  = "${KERNELORG_MIRROR}/linux/network/${BPN}/${BP}.tar.xz \
            file://0001-plugin.h-Change-visibility-to-default-for-debug-symb.patch \
            file://dbus-service.patch \
            file://localstate-to-app-dir.patch \
            file://connman \
            file://main.conf \
            "
SRC_URI[md5sum] = "4a3efdbd6796922db9c6f66da57887fa"
SRC_URI[sha256sum] = "5c5e464bacc9c27ed4e7269fb9b5059f07947f5be26433b59212133663ffa991"

RRECOMMENDS_${PN} = "connman-conf"

do_install_append() {
	install -d ${D}${sysconfdir}/connman
	install -m 0555 ${WORKDIR}/main.conf ${D}${sysconfdir}/connman
}

