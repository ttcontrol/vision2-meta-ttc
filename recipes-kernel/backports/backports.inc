DESCRIPTION = "Latest wireless drivers"
HOMEPAGE = "https://backports.wiki.kernel.org"
SECTION = "kernel/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
RDEPENDS_${PN} = "wireless-tools"
INC_PR = "r0"

# depends on config options which are not enabled in qemu* MACHINEs by default
# config.mk:25: *** "ERROR: your kernel has CONFIG_CFG80211=y, you should have it CONFIG_CFG80211=m if you want to use this thing.".  Stop.
EXCLUDE_FROM_WORLD = "1"

BACKPORTS_VERSION = "${PV}"
BACKPORTS_VERSION_1 = "${BACKPORTS_VERSION}-1"
SHRT_VER = "${@d.getVar('PV',1).split('.')[0]}.${@d.getVar('PV',1).split('.')[1]}"

SRC_URI = "https://www.kernel.org/pub/linux/kernel/projects/backports/stable/v${BACKPORTS_VERSION}/backports-${BACKPORTS_VERSION_1}.tar.xz"

S = "${WORKDIR}/backports-${BACKPORTS_VERSION_1}"

PACKAGES += "${PN}-udev"
FILES_${PN}-udev += "${base_libdir}/udev/* ${sysconfdir}/*"
RDEPEDNDS_${PN}-udev += "udev"

inherit module

EXTRA_OEMAKE = "KLIB_BUILD=${STAGING_KERNEL_DIR} KLIB=${D}"

module_do_compile() {
    unset CC LD
    unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
    oe_runmake KERNEL_PATH=${STAGING_KERNEL_DIR}   \
           KERNEL_SRC=${STAGING_KERNEL_DIR}    \
           KERNEL_VERSION=${KERNEL_VERSION}    \
           ${MAKE_TARGETS}
}

do_configure_append() {
    cp ${WORKDIR}/defconfig ${S}/.config
}

do_install() {
    oe_runmake DEPMOD=echo DESTDIR="${D}" INSTALL_MOD_PATH="${D}" LDFLAGS="" install

    # install udev rules and script
    install -d ${D}${base_libdir}/udev
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0755 udev/compat_firmware.sh ${D}${base_libdir}/udev/compat_firmware.sh
    install -m 0644 udev/50-compat_firmware.rules ${D}${sysconfdir}/udev/rules.d/50-compat_firmware.rules

    # change the SUBSYSTEM from "compat_firmware" to "firmware", as in the Vision2 kernel it's called "firmware"
    sed -i 's/SUBSYSTEM=="compat_firmware"/SUBSYSTEM=="firmware"/g' ${D}${sysconfdir}/udev/rules.d/50-compat_firmware.rules
}
