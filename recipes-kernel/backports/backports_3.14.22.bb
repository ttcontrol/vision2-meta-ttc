include backports.inc
PR = "${INC_PR}.0"

SRC_URI += " \
	    file://20150521-fix-compilation-for-vision2-and-wl12xx.patch \
	    file://20150521-wl12xx-fix-powerdown-up.patch \
        file://20150522-fix-compilation.patch \
        file://20150611-wl12xx-suspend-resume-bugfix.patch \
        file://defconfig \
	   "

SRC_URI[md5sum] = "c502b13149d4b5de5897786deff676da"
SRC_URI[sha256sum] = "a1b6a03647624545d77559db7cc33027aa4dcd882b48247287697dc6a255e3ac"

