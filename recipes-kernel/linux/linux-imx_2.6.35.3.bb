# Copyright (C) 2011-2013 Freescale Semiconductor
# Released under the MIT license (see COPYING.MIT for the terms)

PR = "${INC_PR}.24"

include linux-imx.inc

COMPATIBLE_MACHINE = "(mx5)"

SRC_URI = "${TTC_GIT}/vision2-linux.git;protocol=${TTC_GIT_PROTO} \
           file://defconfig \
           file://perf-avoid-use-sysroot-headers.patch \
	"

SRCREV = "e6d172383b1a4d667dc55e3e51e792f0d7567feb"
