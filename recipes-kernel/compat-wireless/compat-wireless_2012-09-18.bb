include compat-wireless.inc
PR = "${INC_PR}.0"

SRC_URI += "file://20150209-build_wlcore_sdio_for_2.6.35.patch \
	    file://20150209-do-not-compile-kfifo.patch \
	    file://20150209-wl12xx-power-fixes.patch \
	   "

SRC_URI[md5sum] = "1b326731d15177e76601b90a168afef4"
SRC_URI[sha256sum] = "2c5f86dad0ddff266a974cc7928fde95fdb769e00af60ebaa15ff5c64302797f"
