DESCRIPTION = "Latest wireless drivers"
HOMEPAGE = "http://wireless.kernel.org/en/users/Download"
SECTION = "kernel/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYRIGHT;md5=d7810fab7487fb0aad327b76f1be7cd7"
RDEPENDS_${PN} = "wireless-tools"
INC_PR = "r1"

# depends on config options which are not enabled in qemu* MACHINEs by default
# config.mk:25: *** "ERROR: your kernel has CONFIG_CFG80211=y, you should have it CONFIG_CFG80211=m if you want to use this thing.".  Stop.
EXCLUDE_FROM_WORLD = "1"

COMPAT_WIRELESS_VERSION = "${PV}"
SHRT_VER = "${@d.getVar('PV',1).split('.')[0]}.${@d.getVar('PV',1).split('.')[1]}"

SRC_URI = " \
    ${TTC_MIRROR}/${PN}-${COMPAT_WIRELESS_VERSION}.tar.bz2 \
"

S = "${WORKDIR}/compat-wireless-${COMPAT_WIRELESS_VERSION}"

PACKAGES += "${PN}-udev"
FILES_${PN}-udev += "${base_libdir}/udev/* ${sysconfdir}/*"
RDEPEDNDS_${PN}-udev += "udev"

inherit module

EXTRA_OEMAKE = "KLIB_BUILD=${STAGING_KERNEL_DIR} KLIB=${D}"

do_configure_append() {
    ./scripts/driver-select wl12xx
    sed -i "s#@./scripts/update-initramfs## " Makefile
}

do_install() {
    oe_runmake DEPMOD=echo DESTDIR="${D}" INSTALL_MOD_PATH="${D}" LDFLAGS="" install-modules

    # install udev rules and script
    install -d ${D}${base_libdir}/udev
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0755 udev/compat_firmware.sh ${D}${base_libdir}/udev/compat_firmware.sh
    install -m 0644 udev/50-compat_firmware.rules ${D}${sysconfdir}/udev/rules.d/50-compat_firmware.rules

    # change the SUBSYSTEM from "compat_firmware" to "firmware", as in the Vision2 kernel it's called "firmware"
    sed -i 's/SUBSYSTEM=="compat_firmware"/SUBSYSTEM=="firmware"/g' ${D}${sysconfdir}/udev/rules.d/50-compat_firmware.rules
}
