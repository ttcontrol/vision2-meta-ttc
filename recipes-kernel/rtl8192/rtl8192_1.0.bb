DESCRIPTION = "Realtek RTL8192 Wifi drivers"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://include/autoconf.h;endline=19;md5=f8d10a6bd2fdfa240c0634a7c660c57f"
inherit module
PR = "r0"
SRC_URI = "file://rtl8188C_8192C_usb_linux_v4.0.2_9000.20130911.tar.gz \
           file://20150715-makefile-for-vision2.patch \
          "

S = "${WORKDIR}/rtl8188C_8192C_usb_linux_v4.0.2_9000.20130911"
