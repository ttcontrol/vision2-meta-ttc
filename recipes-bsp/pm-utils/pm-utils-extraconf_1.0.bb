SUMMARY = "Extra machine specific configuration files"
DESCRIPTION = "Extra machine specific configuration files for pm-sleep."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r0"

SRC_URI = " \
       file://70custom-modules \
"


do_install() {
    install -d ${D}${sysconfdir}/pm/sleep.d/

    install -m 0755 ${WORKDIR}/70custom-modules  ${D}${sysconfdir}/pm/sleep.d/
}

FILES_${PN} = "${sysconfdir}/pm"
RDEPENDS_${PN} = "pm-utils"
