
do_install_append () {
    # Remove all "rules" provided by pm-utils. The Vision2 does not
    # need them
    rm -rf ${D}${libdir}/pm-utils/sleep.d/*
}
