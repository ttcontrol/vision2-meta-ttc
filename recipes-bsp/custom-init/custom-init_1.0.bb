DESCRIPTION = "TTTech custom initialization process - in replaces standard /sbin/init"
SECTION = "Base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

SRC_URI = "file://${PN}_${PV}.tar.bz2"
S = "${WORKDIR}/${PN}"

FILES_${PN} = "${base_sbindir}/init ${sysconfdir}/profile.d/env.sh"

do_install () {
	install -d ${D}/${base_sbindir}
	install -m 755 bin/custom-init ${D}/${base_sbindir}/init
	${STRIP} ${D}/${base_sbindir}/init
	install -d ${D}${sysconfdir}/profile.d/
	install -m 0755 bin/env.sh ${D}${sysconfdir}/profile.d/
}

