require recipes-bsp/u-boot/u-boot.inc

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://README;beginline=1;endline=22;md5=3a00ef51d3fc96e9d6c1bc4708ccd3b5"

PR = "r1"
PV = "v2010.06+git${SRCPV}"

# This revision corresponds to the tag "vision2-1.14.0.0"
# We use the revision in order to avoid having to fetch it from the repo during parse
SRCREV = "0cd7d7c3ae72e6cbe77fe9793d282065440ca863"

SRC_URI = "${TTC_GIT}/vision2-u-boot.git;protocol=${TTC_GIT_PROTO} \
           file://u-boot.env \
           file://update-u-boot.env"

# bootloader code might depend on OpenSSL (depending on config options),
# image handling related user space tools unconditionally depend on OpenSSL
DEPENDS += " openssl openssl-native "

# provide additional make parameters to help build the user space tools;
# note that the native.bbclass approach does not work here, the phrase
# HOSTCC="${CCACHE}${HOST_PREFIX}gcc ${HOST_CC_ARCH}" results in target specs;
# note as well that U-Boot v2014.01 requires the CFLAGS in the HOSTCC spec,
# passing separate HOST_CFLAGS only became operational in version v2014.04
EXTRA_OEMAKE += ' \
   HOST_TOOLS_ALL=n \
   HOSTCC="gcc ${BUILD_CFLAGS}" \
   HOSTLD="ld" \
   HOSTLDFLAGS="${BUILD_LDFLAGS}" \
   HOSTSTRIP=true '


S = "${WORKDIR}/git"

FILESDIR = "${@os.path.dirname(d.getVar('FILE',1))}/u-boot-git/${MACHINE}"

PACKAGE_ARCH = "${MACHINE_ARCH}"

UBOOT_SRC_PATH = "/usr/src/u-boot"

FILES_${PN}-dev = "${UBOOT_SRC_PATH}"

do_compile_prepend_vision2 () {
  # the first call of "make vision2_config" for some reason fails.
  # As workaround, add an additional call of "make vision2_config"
  oe_runmake vision2_config
}

do_install_append () {

    ubootsrcdir=${D}${UBOOT_SRC_PATH}

    install -d $ubootsrcdir

    #
    # Copy the entire source tree. In case an external build directory is
    # used, copy the build directory over first, then copy over the source
    # dir. This ensures the original Makefiles are used and not the
    # redirecting Makefiles in the build directory.

    cp -fR * $ubootsrcdir
    if [ "${S}" != "${B}" ]; then
        cp -fR ${S}/* $ubootsrcdir
    fi
    oe_runmake -C $ubootsrcdir distclean

}

do_deploy_append_vision2 () {
  install -m 0644 ${WORKDIR}/u-boot.env ${DEPLOY_DIR_IMAGE}
  install -m 0644 ${WORKDIR}/update-u-boot.env ${DEPLOY_DIR_IMAGE}
}
