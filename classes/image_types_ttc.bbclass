
inherit image_types

IMAGE_TYPES += "v2update"

IMAGE_DEPENDS_v2update = "mtd-utils-native"

check_return() {
        if [ "$1" -ne "0" ]; then
                echo
                echo "ERROR: Failed with code $1"
                echo
                exit $1
        fi
}


IMAGE_CMD_v2update () {
        KERNEL_FILE=`readlink ${DEPLOY_DIR_IMAGE}/fImage`
        check_return $?

        FSIMAGE_ROOTFS=rootfs.ubifs
        FSIMAGE_APP=application.ubifs

        RELEASE_INFO_FILE="${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.info"

        echo "UPDATER_RELEASE=${UPDATER_RELEASE}" > $RELEASE_INFO_FILE
        echo "SW_CLASS=${SW_CLASS}" >> $RELEASE_INFO_FILE
        echo "HW_CLASS_0=${HW_CLASS_0}" >> $RELEASE_INFO_FILE
        echo "HW_CLASS_1=${HW_CLASS_1}" >> $RELEASE_INFO_FILE
        echo "HW_CLASS_2=${HW_CLASS_2}" >> $RELEASE_INFO_FILE
        echo "HW_CLASS_3=${HW_CLASS_3}" >> $RELEASE_INFO_FILE
        echo "HW_CLASS_4=${HW_CLASS_4}" >> $RELEASE_INFO_FILE
        
        # for each image the comments and names represent/encode the following parameters:
        # total device block count - DBC
        # block size - BS
        # page size - PS
        # subpage size - SPS
        # example: ${IMAGE_LINK_NAME}_DBC_BS_PS_SPS.extension (.cfg/.ubi)

        ln -sf ${RELEASE_INFO_FILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}.info

        # ==== 2048_512K_4K_1024 ====
        # MT29F8G08ABABAWP

        CFGFILE=ubinize_2048_512K_4K_1024.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=762MiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE


        UBI_IMAGE_NAME="${IMAGE_NAME}_2048_512K_4K_1024"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"
        XMLFILE="${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.xml"

        echo "<Config>" > $XMLFILE
        echo "  <Releases>" >> $XMLFILE
        echo "    <Release version=\"${UPDATER_RELEASE}\" ubiPath=\"$UBI_FILE_NAME\" kernelPath=\"$KERNEL_FILE\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"${HW_CLASS_0}\" sw-class=\"${SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS}  -m 4096 -e 520192 -c 2004 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 4096 -e 520192 -c 2004 -v -x none -U && \
        ubinize -o ${OUTFILE} -m 4096 -p 512KiB -s 1024 $CFGFILE
        check_return $?

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_2048_512K_4K_1024.ubi

        # ==== 4096_128K_2K_512 ====
        # MT29F4G08ABADAWP, FMND4GxxU3J

        CFGFILE=ubinize_4096_128K_2K_512.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=250MiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE
        UBI_IMAGE_NAME="${IMAGE_NAME}_4096_128K_2K_512"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"

        echo "    <Release version=\"${UPDATER_RELEASE}\" ubiPath=\"$UBI_FILE_NAME\" kernelPath=\"$KERNEL_FILE\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"${HW_CLASS_1}\" sw-class=\"${SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS} -m 2048 -e 129024 -c 3936 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 2048 -e 129024 -c 3936 -v -x none -U  && \
        ubinize -o ${OUTFILE} -m 2048 -p 128KiB -s 512 $CFGFILE
        check_return $?

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_4096_128K_2K_512.ubi

        # ==== 8192_128K_2K_512 ====
        # FMND8D08U3K, FMND8D08U3J

        CFGFILE=ubinize_8192_128K_2K_512.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=774296KiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE
        UBI_IMAGE_NAME="${IMAGE_NAME}_8192_128K_2K_512"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"

        echo "    <Release version=\"${UPDATER_RELEASE}\" ubiPath=\"$UBI_FILE_NAME\" kernelPath=\"$KERNEL_FILE\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"${HW_CLASS_2}\" sw-class=\"${SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS} -m 2048 -e 129024 -c 1752 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 2048 -e 129024 -c 6192 -v -x none -U  && \
        ubinize -o ${OUTFILE} -m 2048 -p 128KiB -s 512 $CFGFILE
        check_return $?

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_8192_128K_2K_512.ubi

        # ==== 4096_128K_2K_2048 ====
        # K9F4G08U0F

        CFGFILE=ubinize_4096_128K_2K_2048.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=260276KiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE
        UBI_IMAGE_NAME="${IMAGE_NAME}_4096_128K_2K_2048"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"

        echo "    <Release version=\"${UPDATER_RELEASE}\" ubiPath=\"$UBI_FILE_NAME\" kernelPath=\"$KERNEL_FILE\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"${HW_CLASS_3}\" sw-class=\"${SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS} -m 2048 -e 126976 -c 1872 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 2048 -e 126976 -c 3900 -v -x none -U  && \
        ubinize -o ${OUTFILE} -m 2048 -p 131072 -s 2048 $CFGFILE
        check_return $?

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_4096_128K_2K_2048.ubi

        # ==== 8192_128K_2K_2048 ====
        # K9K8G08U0F

        CFGFILE=ubinize_8192_128K_2K_2048.cfg
        echo "[application-volume]" > $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=1" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=application" >> $CFGFILE
        echo "vol_size=787287KiB" >> $CFGFILE
        echo "image=$FSIMAGE_APP" >> $CFGFILE
        echo "[rootfs-volume]" >> $CFGFILE
        echo "mode=ubi" >> $CFGFILE
        echo "vol_id=2" >> $CFGFILE
        echo "vol_type=dynamic" >> $CFGFILE
        echo "vol_name=rootfs" >> $CFGFILE
        echo "vol_flags=autoresize" >> $CFGFILE
        echo "image=$FSIMAGE_ROOTFS" >> $CFGFILE
        UBI_IMAGE_NAME="${IMAGE_NAME}_8192_128K_2K_2048"
        UBI_FILE_NAME="${UBI_IMAGE_NAME}.rootfs.ubi"
        OUTFILE="${DEPLOY_DIR_IMAGE}/${UBI_FILE_NAME}"

        echo "    <Release version=\"${UPDATER_RELEASE}\" ubiPath=\"$UBI_FILE_NAME\" kernelPath=\"$KERNEL_FILE\">" >> $XMLFILE
        echo "      <Compatibility hw-class=\"${HW_CLASS_4}\" sw-class=\"${SW_CLASS}\"/>" >> $XMLFILE
        echo "    </Release>" >> $XMLFILE
        echo "  </Releases>" >> $XMLFILE
        echo "</Config>" >> $XMLFILE

        mkfs.ubifs -r ${IMAGE_ROOTFS} -o ${FSIMAGE_ROOTFS} -m 2048 -e 126976 -c 2048 -v -x none -U && \
        mkfs.ubifs -o ${FSIMAGE_APP}  -m 2048 -e 126976 -c 8192 -v -x none -U  && \
        ubinize -o ${OUTFILE} -m 2048 -p 131072 -s 2048 $CFGFILE
        check_return $?

        ln -sf ${OUTFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}_8192_128K_2K_2048.ubi

        ln -sf ${XMLFILE} ${DEPLOY_DIR_IMAGE}/${IMAGE_LINK_NAME}.xml

}
