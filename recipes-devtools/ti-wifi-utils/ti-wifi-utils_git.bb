require ti-wifi-utils.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=4725015cb0be7be389cf06deeae3683d"

SRC_URI += "file://20150915-fix-tx_bip-command.patch"

PR_append ="b+gitr${SRCPV}"

SRCREV = "1971f622534da197994b3af6b8a73d38c75f768e"
