DESCRIPTION = "Hardware Abstraction layer for TTControl Vision2"
SECTION = "Base"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"
PR = "r1"

SRC_URI = "${TTC_GIT}/libhal.git;protocol=${TTC_GIT_PROTO}"
SRCREV = "8761ce0995d27e6ee97c545871ca7c7ed63620e0"

S = "${WORKDIR}/git"

DEPENDS = "glib-2.0 libevent gpsd gstreamer gst-fsl-plugin udev dbus"
RDEPENDS_${PN} = "glib-2.0 gpsd gstreamer gst-fsl-plugin udev"

inherit autotools pkgconfig

FILES_${PN} += "${base_libdir}/udev/*"
