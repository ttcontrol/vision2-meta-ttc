require libroxml.inc
PR = "${INC_PR}.0"

LIC_FILES_CHKSUM = "file://License.txt;md5=81cba52d2de829c8be3d167618e6b8b6"

SRC_URI[md5sum] = "a975f91be150f7a19168a45ce15769ca"
SRC_URI[sha256sum] = "1da8f20b530eba4409f2b217587d2f1281ff5d9ba45b24aeac71b94c6c621b78"
