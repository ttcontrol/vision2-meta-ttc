DESCRIPTION = "Basic TTControl specific image with various \
               utilities, drivers and clutter support"

include ttc-image-base.bb

IMAGE_INSTALL += "mx \
gstreamer clutter-gst clutter-ply \
gst-fsl-plugin gst-fsl-plugin-libgstfsl \
gst-plugins-good-udp gst-plugins-good-rtp gst-plugins-good-rtpmanager gst-plugins-base-videotestsrc \
firmware-imx-vpu-imx51 firmware-imx-sdma-imx51 \
gdk-pixbuf-loader-jpeg \
gdk-pixbuf-loader-tga \
gdk-pixbuf-loader-gif \
gdk-pixbuf-loader-ani \
gdk-pixbuf-loader-ras \
gdk-pixbuf-loader-pnm \
gdk-pixbuf-loader-xpm \
gdk-pixbuf-loader-ico \
gdk-pixbuf-loader-pcx \
gdk-pixbuf-loader-xbm \
gdk-pixbuf-loader-png \
gdk-pixbuf-loader-icns \
gdk-pixbuf-loader-wbmp \
gdk-pixbuf-loader-bmp \
gdk-pixbuf-loader-qtif \
ttf-dejavu wqy-microhei \
ttf-bitstream-vera \
htmldoc \
unzip zip \
curl strace \
mgetty \
ppp \
ppp-extraconf \
libroxml \
libhal \
libnl iptables iptables-conf \
backports backports-udev \
rtl8192 \
linux-firmware-wl12xx linux-firmware-ath9k \
linux-firmware-ar9170 linux-firmware-ralink \
wireless-tools wpa-supplicant connman connman-client \
ethtool fbvncserver \
"
