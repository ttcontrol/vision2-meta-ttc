DESCRIPTION = "Base TTControl image"

include ttc-image-minimal.bb

IMAGE_INSTALL += "mtd-utils-ubifs mtd-utils-nor \
		  dropbear iproute2 \
		  can-utils u-boot-fw-utils \
          memtester \
		  tslib-tests tslib-calibrate \
		  libgles-mx51 lib2dz430-mx51  \
		  v2-sysutils tzdata udev-extraconf \
          evtest mxt-app \
		 "


# Set root password to "dude"
ROOTFS_POSTPROCESS_COMMAND += "sed -i 's%^root:[^:]*:%root:$1$OgcLKswK$fjzA2Rf4U5a2vUkwfiiWW/:%' ${IMAGE_ROOTFS}/etc/shadow ;"

# Set ldconfig config file
# TODO: split content and add to recipies? (gstreamer, tslib, ...)
ROOTFS_POSTPROCESS_COMMAND += "echo /rts\\n/usr/lib/ts\\n/lib\\n/usr/lib/gstreamer-0.10\\n >> ${IMAGE_ROOTFS}/etc/ld.so.conf ;"
