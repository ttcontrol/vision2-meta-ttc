include dbus.inc

PR = "${INC_PR}.0"

SRC_URI += "file://reduce-connection-dispatch-recheck-timeout.patch \
"

SRC_URI[md5sum] = "27b8e99ffad603b8acfa25201c6e3d5c"
SRC_URI[sha256sum] = "ad7dcad73ad9b0ff55819985d354eacfffe07e2eb8c763e155efc21d6001084b"

