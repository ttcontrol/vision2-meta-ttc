#!/bin/sh

[ $# -eq 0 ] && { echo "Usage: $0 <interface name>"; exit 1; }

[ -f /etc/iptables/custom.in ] && sed "s/__INTERFACE__/$1/" /etc/iptables/custom.in | iptables-restore -n
[ -f /etc/iptables/default.in ] && sed "s/__INTERFACE__/$1/" /etc/iptables/default.in | iptables-restore -n
