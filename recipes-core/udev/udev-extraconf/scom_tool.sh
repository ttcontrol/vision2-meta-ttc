#!/bin/sh


VAR_RUN_FILE=/var/run/scom_tool

if [ -e $VAR_RUN_FILE ]; then
        log "udev rule already executed once! Exiting..."
        exit 0
fi

proc_count=`ps | grep -o "scom_tool -p" | wc -l`

if [ [ "$proc_count" -eq 2 ] ]; then
        log "udev rule executed as a result of the EHS8 production tool! Exiting..."
        exit 0
fi

touch $VAR_RUN_FILE

/usr/bin/scom_tool &
