#!/bin/sh

log ()                                                  
{
        logger -t updater_udev "$1"
}

mount_and_check ()
{
        mount -t vfat -o shortname="winnt",sync $1 ${MOUNTPATH}
        if [ "$?" -eq '0' ]; then
	        log "Mount successfull: $1 -> $MOUNTPATH"
		trap "umount $MOUNTPATH; exit" TERM KILL

		if [ -e ${UPDATER_SCRIPT} ]; then
			log "Found ${UPDATER_SCRIPT}!"
			log "Executing ${UPDATER_SCRIPT} ${MOUNTPATH} $1"
                	sh ${UPDATER_SCRIPT} $1 ${MOUNTPATH} &
			if [ "$?" != "0" ]; then
				log "Cannot start updater!"
				umount $MOUNTPATH; rmdir $MOUNTPATH;
				return 0;
			else
				return 1;
			fi
		else
			log "${UPDATER_SCRIPT} not found. Unmounting..."
			umount $MOUNTPATH; rmdir $MOUNTPATH;
			return 0;
        	fi

	else
		log "Mount not successfull: $1 -> $MOUNTPATH"
		return 0;
        fi
}


VAR_RUN_FILE=/var/run/usb-checkforupdates
CUSTOM_INIT_PIPE=/tmp/custom_init_pipe
MOUNTPATH=/mnt/usb-updater    
UPDATER_SCRIPT=${MOUNTPATH}/updater/launch.sh

log "Found new usb block device ($DEVNAME)..."

if [ "$STARTUP" == "1" ]; then
	log "...while booting"
else
	log "...not while booting"
	exit 0
fi

if [ -e $VAR_RUN_FILE ]; then
        log "udev rule already executed once! Exiting..."
        exit 0
fi

mkdir -p ${MOUNTPATH}
touch $VAR_RUN_FILE

PARTITION1=${DEVNAME}1

if [ -e ${PARTITION1} ]; then
	log "Partition ${PARTITION1} found"
	mount_and_check ${PARTITION1}
	UPDATE_FOUND=$?
	
else
	log "Partition ${PARTITION1} not found"
	mount_and_check ${DEVNAME}
	UPDATE_FOUND=$?
	
fi

echo "Update found: $UPDATE_FOUND"
printf "%d\n" "$UPDATE_FOUND" > $CUSTOM_INIT_PIPE

exit 0
