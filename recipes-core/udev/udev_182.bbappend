
do_install_append () {
    rm ${D}${sysconfdir}/udev/rules.d/local.rules
    
    #Delete all default rules except 80-drivers which is needed for automatically
    #loading kernel modules for USB WiFi modules
    find ${D}${base_libdir}/udev/rules.d -type f ! -name '80-drivers.rules' -execdir rm {} +
    
    rm -rf ${D}${base_libdir}/udev/keymaps/*
}
